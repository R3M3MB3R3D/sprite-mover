﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEDQ : MonoBehaviour
{
    public GameObject Cube;
    
	void Start ()
    {
       
	}
	
	void Update ()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Q Sprite Disabled");
            Cube.SetActive(!Cube.activeInHierarchy);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Escape Application Exit");
            Application.Quit();
        }
	}
}