﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private PlayerMove movement;

    // Use this for initialization
    void Start ()
    {
        movement = GetComponent<PlayerMove>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("P Script Disabled or Enabled");
            movement.enabled = !movement.enabled;
        }
    }
}
