﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    //declaring variables and making them editable within the inspector
    public float speed = 5;
    public float rotate = 1000;

    //creating a variable for tracking the sprites original position
    private Vector3 pos;

    void Start()
    {
        //identifying the sprites original position
        pos = gameObject.transform.position;
    }

    void Update()
    {
        //creating movement values based on Time function
        var transAmount = speed * Time.deltaTime;
        var transRotate = speed * Time.deltaTime;

        //creating W Key single and continuous movement condition
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //creates the proper condition for moving the sprite one unit
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("Shift+W Single Movement");
                transform.Translate(0, transAmount, 0);
            }
        }
        //creates the proper condition for moving the sprite continuously
        else if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("W Continuous Movement");
            transform.Translate(0, transAmount, 0);
        }

        //creating S Key single and continuous movement conditions
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("Shift+S Single Movement");
                transform.Translate(0, -transAmount, 0);
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("S Continuous Movement");
            transform.Translate(0, -transAmount, 0);
        }

        //creating A Key single and continuous movement conditions
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("Shift+A Single Movement");
                transform.Translate(-transAmount, 0, 0);
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("A Continuous Movement");
            transform.Translate(-transAmount, 0, 0);
        }

        //creating D Key single and continuous movement conditions
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                Debug.Log("Shift+D Single Movement");
                transform.Translate(transAmount, 0, 0);
            }
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("D Continuous Movement");
            transform.Translate(transAmount, 0, 0);
        }

        //creating Q and E key continuous rotation conditions
        if (Input.GetKey(KeyCode.Tab))
        {
            Debug.Log("Tab Continuous Movement");
            transform.Rotate(0, 0, transRotate);
        }
        if (Input.GetKey(KeyCode.E))
        {
            Debug.Log("E Continuous Movement");
            transform.Rotate(0, 0, -transRotate);
        }

        //creating location reset condition
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Space Used");
            gameObject.transform.position = pos;
        }
    }
}